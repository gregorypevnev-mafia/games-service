const fs = require("fs");
const path = require("path")

const readSecret = secretName => {
  const secretValue = String(process.env[secretName]);

  if (!secretValue) return null;

  if (path.isAbsolute(secretValue)) return fs.readFileSync(secretValue, "utf8");

  return secretValue;
};

module.exports = readSecret;