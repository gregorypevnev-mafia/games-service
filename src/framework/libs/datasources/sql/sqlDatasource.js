const { createSQLClient } = require("./sqlClient");
const { createSQLManager } = require("./sqlManager");

const createSQLDatasource = (name, sqlConfig) => {
  const client = createSQLClient(sqlConfig);
  const manager = createSQLManager(client);

  return {
    name,
    query(query) {
      return query(client);
    },
    repository(repo) {
      return manager.provisionOperations(repo.operations, repo.transactional)
    }
  }
};

module.exports = { createSQLDatasource };
