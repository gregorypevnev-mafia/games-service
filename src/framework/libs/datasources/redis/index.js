const { createRedisDatasource } = require("./redisDatasource");

module.exports = {
  name: "redis",
  creator: createRedisDatasource,
};
