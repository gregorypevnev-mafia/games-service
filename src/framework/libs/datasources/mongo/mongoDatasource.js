const { createMongoClient } = require("./mongoClient");
const { createMongoManager } = require("./mongoManager");

const createMongoDatasource = async (name, mongoConfig) => {
  const { client, db } = await createMongoClient(mongoConfig);
  const manager = createMongoManager(client, db);

  return {
    name,
    query(query) {
      return query(db);
    },
    repository(repo) {
      return manager.provisionOperations(repo.operations, repo.transactional);
    }
  }
};

module.exports = { createMongoDatasource };
