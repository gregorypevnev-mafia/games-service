exports.NOT_FOUND = "NOT_FOUND";

exports.FORBIDDEN = "FORBIDDEN";

exports.ERROR = "ERROR";

exports.UNAUTHED = "UNAUTHED";

exports.INTERNAL = "INTERNAL";
