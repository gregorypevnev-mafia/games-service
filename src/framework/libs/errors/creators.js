const { ERROR, UNAUTHED, FORBIDDEN, NOT_FOUND, INTERNAL } = require("./types");

const newError = type => (message = "", data = null) => {
  throw {
    type,
    message,
    data
  }
};

module.exports = {
  error: newError(ERROR),
  unauthed: newError(UNAUTHED),
  forbidden: newError(FORBIDDEN),
  notFound: newError(NOT_FOUND),
  internal: newError(INTERNAL)
};
