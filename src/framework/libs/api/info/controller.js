const createInfoController = ({ info }) => {
  const serviceInfo = ({ }) => (req, res) => {
    return res.status(200).json(info);
  };

  const healthInfo = ({ }) => (req, res) => {
    return res.status(200).send("Healthy");
  };

  return {
    path: "/info",
    middleware: [],
    use: [
      {
        path: "/service",
        use: {
          controller: serviceInfo,
        }
      },
      {
        path: "/health",
        use: {
          controller: healthInfo,
        }
      },
    ]
  }
};

module.exports = { createInfoController };
