const yup = require("yup");

const signupSchema = yup.object({
  username: yup.string()
    .max(20, "Username is too long")
    .required("Username is required"),

  password: yup.string()
    .min(3, "Password is too short")
    .max(50, "Password is too long")
    .required("Password is required"),
});

module.exports = {
  name: "signup",
  schema: signupSchema,
};