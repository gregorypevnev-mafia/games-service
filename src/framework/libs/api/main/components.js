const combineMiddleware = (components, dependencies) =>
  components.reduce(
    (mainMiddleware, { middleware }) => {
      return {
        ...mainMiddleware,
        ...middleware.reduce((mapped, { name, handler }) => ({
          ...mapped,
          [name]: handler(dependencies),
        }), {})
      }
    }, {});

const combineControllers = components =>
  components.reduce(
    (mainController, { controller }) => {
      return {
        ...mainController,
        use: controller ? [
          ...mainController.use,
          controller,
        ] : mainController.use
      }
    }, {
    path: "/",
    use: [],
  });

const categorizeComponents = components =>
  components.reduce(({ apiComponents, wsComponents }, component) => {
    if (component.ws) return { apiComponents, wsComponents: [...wsComponents, component] };

    return { apiComponents: [...apiComponents, component], wsComponents };
  }, {
    apiComponents: [],
    wsComponents: [],
  });

const combineComponents = (components, dependencies) => {
  const middleware = combineMiddleware(components, dependencies); // Middleware is common / shared
  const { apiComponents, wsComponents } = categorizeComponents(components);

  const api = combineControllers(apiComponents);
  const ws = combineControllers(wsComponents);

  return {
    middleware,
    api,
    ws,
  }
};

module.exports = { combineComponents };
