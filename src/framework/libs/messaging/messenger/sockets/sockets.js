const createSockets = ({
  onMessage,
  onClose
}) => {
  const sockets = {};

  const remove = socketId => {
    if (!sockets[socketId]) return;

    sockets[socketId].close();

    delete sockets[socketId];
  };

  const add = (socket, socketId, details) => {
    socket.id = socketId;
    socket.details = details;

    sockets[socketId] = socket;

    socket.on("message", message => {
      onMessage(JSON.parse(message), socketId, details);
    });

    socket.on("close", () => {
      remove(socketId);

      onClose(socketId, details);
    });
  };

  const sendToSocket = (socketId, message) => new Promise((res, rej) => {
    const socket = sockets[socketId];

    if (!socket) return res();

    socket.send(JSON.stringify(message), {}, res);
  });

  const send = (socketIds, message) => Promise.all(
    socketIds.map(socketId => sendToSocket(socketId, message))
  );

  const broadcast = message => send(
    Array.from(Object.keys(sockets)),
    message,
  );

  return {
    add,
    remove,
    send,
    broadcast,
  };
}

module.exports = { createSockets };
