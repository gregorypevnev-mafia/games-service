const { microserviceBuilder } = require("./framework");

(async function () {
  const microservice = await microserviceBuilder()
    .withEvents(require("./app/events.json"))
    .addCommands(require("./app/commands"))
    .addEventHandlers(require("./app/event-handlers"))
    .addQueries(require("./app/queries"))
    .addRepositories(require("./app/repositories"))
    .addServices(require("./app/services"))
    .withApi(require("./app/api"))
    .withSchemas(require("./app/schemas"))
    .useInfo()
    .build();

  microservice.start();
}());
