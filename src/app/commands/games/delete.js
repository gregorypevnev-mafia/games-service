const type = "delete-game";

const handler = ({
  errors,
  events,
  repositories: { games: gamesRepo },
  services: { games: gamesService, formatter }
}) => async ({ gameId, user }) => {
  const game = await gamesRepo.find(gameId);

  if (game === null) errors.notFound("Game not found");

  if (game.by.id !== user.id) errors.forbidden("No permission to delete the game");

  if (!gamesService.isCancelled(game)) errors.error("Game is not in the state to be deleted");

  try {
    await gamesRepo.remove(gameId);

    await events.emit("game-deleted", {
      game: formatter.gameChanged(game),
    });

    return game;
  } catch (e) {
    errors.error("Could not delete game");
  }
}

module.exports = {
  type,
  handler
};