const type = "create-game";

const handler = ({
  errors,
  events,
  repositories: { games: gamesRepo },
  services: { ids, time, games: gamesService, formatter }
}) => async ({
  game: {
    name,
    invitations,
    settings
  },
  user
}) => {
    try {
      const gameData = gamesService.emptyGame(name, user, invitations, settings);

      const game = {
        id: ids.generate(),
        ...gameData,
        at: time.timestamp(),
      };

      await gamesRepo.create(game);

      await events.emit("game-created", {
        game: formatter.gameChanged(game),
      });

      return formatter.game(game);
    } catch (e) {
      errors.error(e.message || "Could not create game");
    }
  };

module.exports = {
  type,
  handler
};