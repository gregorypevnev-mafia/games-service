const isWaiting = ({ waiting }, { id }) =>
  waiting.indexOf(id) !== -1;

const isReady = ({ ready }, { id }) =>
  ready.find(player => player.id === id);

const verifyGame = errors => user => game => {
  if (game === null) errors.notFound("Invitation not found");

  if (isReady(game, user)) errors.error("Already a player");

  if (!isWaiting(game, user)) errors.error("Not invited");

  return game;
};

module.exports = { verifyGame };
