const { verifyGame } = require("./utils/verifyGame");

const type = "accept-invitation";

const handler = ({
  errors,
  events,
  repositories: { games: gamesRepo, members: membersRepo },
  services: { formatter: formatterService, games: gamesService },
}) => async ({ gameId, user }) => {
  const game = await gamesRepo.find(gameId).then(verifyGame(errors)(user));

  try {
    await membersRepo.addReady(game.id, user.id, gamesService.newPlayer(user));

    await events.emit("invitation-accepted", formatterService.invitationModified(game, user));
  } catch (e) {
    errors.error("Could not accept invitation");
  }
};

module.exports = {
  type,
  handler
};