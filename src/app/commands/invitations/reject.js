const { verifyGame } = require("./utils/verifyGame");

const type = "reject-invitation";

const handler = ({
  errors,
  events,
  repositories: { games: gamesRepo, members: membersRepo },
  services: { formatter },
}) => async ({ gameId, user }) => {
  const game = await gamesRepo.find(gameId).then(verifyGame(errors)(user));

  try {
    await membersRepo.removeWaiting(game.id, user.id);

    await events.emit("invitation-rejected", formatter.invitationModified(game, user));
  } catch (e) {
    errors.error("Could not reject invitation");
  }
};

module.exports = {
  type,
  handler
};