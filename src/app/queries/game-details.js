const type = "game-details";
const datasource = "database";

// Only returning data when the game is ready
//  - All players confirmed
//  - Details configured
const handler = mongodb => ({
  errors,
  services: { games: gamesService, formatter }
}) => async gameId => {
  const game = await mongodb.collection("games").findOne({
    _id: gameId,
  });

  if (game === null) return null;

  if (!gamesService.isPlaying(game)) errors.error("Game has not been started");

  return formatter.gameDetails(game);
}

module.exports = {
  type,
  datasource,
  handler,
};