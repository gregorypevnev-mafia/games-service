module.exports = [
    require('./game-details'),
    require("./game-info"),
    require('./games-list'),
    require('./invitations-list')
];