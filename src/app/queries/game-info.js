const type = "game-info";
const datasource = "database";

const handler = mongodb => ({
  services: { formatter }
}) => async gameId => {
  const game = await mongodb.collection("games").findOne({
    _id: gameId,
  });

  return game === null ? null : formatter.game(game);
}

module.exports = {
  type,
  datasource,
  handler,
};