const type = "games-list";
const datasource = "database";

const handler = mongodb => ({
  services: { formatter }
}) => async ({ id }) =>
    mongodb.collection("games")
      .find({ "ready.id": id })
      .sort({ at: -1 })
      .toArray()
      .then(formatter.games);

module.exports = {
  type,
  datasource,
  handler,
};