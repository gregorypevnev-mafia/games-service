const type = "invitations-list";
const datasource = "database";

const handler = mongodb => ({
  services: { formatter }
}) => async ({ id }) =>
    mongodb.collection("games")
      .find({
        waiting: id,
        status: "WAITING", // Filtering out cancelled invitations
      })
      .sort({ at: -1 })
      .toArray()
      .then(formatter.invitations);

module.exports = {
  type,
  datasource,
  handler,
};