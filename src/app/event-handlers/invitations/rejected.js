const type = "invitation-rejected";

const handler = ({
  events,
  repositories: { games: gamesRepo },
  services: { games: gamesService, formatter }
}) => async ({ game: rejectedGame }) => {
  const game = await gamesRepo.find(rejectedGame.id);

  await gamesRepo.update(game.id, gamesService.cancelGame());

  await events.emit("game-cancelled", {
    game: formatter.gameChanged(game),
  });
};

module.exports = {
  type,
  handler
}