const type = "invitation-accepted";

const handler = ({
  events,
  repositories: { games: gamesRepo },
  services: { games: gamesService, formatter }
}) => async ({ game: acceptedGame }) => {
  const game = await gamesRepo.find(acceptedGame.id);

  if (gamesService.isReady(game)) {
    const startedGame = gamesService.startGame(game);

    await gamesRepo.update(game.id, startedGame);

    await events.emit("game-started", {
      game: formatter.gameChanged(game),
    });
  }
};

module.exports = {
  type,
  handler
}