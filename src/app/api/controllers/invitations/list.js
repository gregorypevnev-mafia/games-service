const list = ({ queries }) => async (req, res) => {
  const invitations = await queries.query("invitations-list", req.user);

  return res.status(200).json({ invitations });
};

module.exports = {
  path: "/invitations",
  middleware: [],
  auth: true,
  private: true,
  use: {
    method: "GET",
    controller: list,
  },
};
