const remove = ({ commands }) => async (req, res) => {
  const id = String(req.params.id);

  await commands.call("reject-invitation", {
    gameId: id,
    user: req.user,
  });

  return res.status(204).send();
};

const update = ({ commands, queries }) => async (req, res) => {
  const id = String(req.params.id);

  await commands.call("accept-invitation", {
    gameId: id,
    user: req.user,
  });

  const game = await queries.query("game-info", id);

  return res.status(200).json({ game });
};

module.exports = {
  path: "/invitations",
  middleware: [],
  auth: true,
  private: true,
  use: [
    {
      path: "/:id",
      middleware: [],
      use: {
        method: "DELETE",
        controller: remove,
      }
    }, {
      path: "/:id",
      middleware: [],
      use: {
        method: "PUT",
        controller: update,
      }
    },
  ],
};
