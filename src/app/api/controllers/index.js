module.exports = {
  path: "/",
  use: [
    require("./games/details"),
    require("./games/data"),
    require("./invitations/list"),
    require("./invitations/modifications")
  ],
};