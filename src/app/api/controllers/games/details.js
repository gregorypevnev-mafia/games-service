const find = ({ queries }) => async (req, res) => {
  const id = String(req.params.id);

  const game = await queries.query("game-details", id);

  if (game === null)
    return res.status(404).json({
      error: {
        message: "Game not found"
      }
    })

  return res.status(200).json({ game });
};

module.exports = {
  path: "/games",
  middleware: [],
  use: [
    {
      path: "/:id",
      middleware: [],
      use: {
        method: "GET",
        controller: find,
      }
    }
  ],
};
