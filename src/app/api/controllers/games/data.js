const list = ({ queries }) => async (req, res) => {
  const games = await queries.query("games-list", req.user);

  return res.status(200).json({ games });
};

const create = ({ commands }) => async (req, res) => {
  const { settings, details, ...game } = await commands.call("create-game", {
    game: req.body,
    user: req.user
  });

  return res.status(201).json({ game });
};

const find = ({ queries }) => async (req, res) => {
  const id = String(req.params.id);

  const game = await queries.query("game-details", id);

  if (game === null)
    return res.status(404).json({
      error: {
        message: "Game not found"
      }
    })

  return res.status(200).json({ game });
};

const remove = ({ commands }) => async (req, res) => {
  const id = String(req.params.id);

  await commands.call("delete-game", {
    gameId: id,
    user: req.user,
  });

  return res.status(204).send();
};

module.exports = {
  path: "/games",
  middleware: [],
  auth: true,
  private: true,
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "GET",
        controller: list,
      }
    }, {
      path: "/",
      middleware: [],
      use: {
        method: "POST",
        controller: create,
      },
      schema: "game-data"
    }, {
      path: "/:id",
      middleware: [],
      use: {
        method: "DELETE",
        controller: remove,
      }
    },
  ],
};
