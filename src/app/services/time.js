const moment = require("moment");

const name = "time";

const timestamp = () => () => moment().valueOf();

module.exports = {
  name,
  functions: {
    timestamp,
  }
};
