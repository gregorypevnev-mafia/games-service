const { v1: uuid } = require("uuid");

const name = "ids";

const generate = ({ }) => () => String(uuid());

module.exports = {
  name,
  functions: {
    generate,
  }
};