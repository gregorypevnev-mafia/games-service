const { games, roles } = require("../mafia-game");

const name = "games";

// Statuses are NOT a part of the game-logic
const STATUSES = {
  waiting: "WAITING",
  playing: "PLAYING",
  cancelled: "CANCELLED",
  finished: "FINISHED",
};

const isCancelled = () => ({ status }) => status === STATUSES.cancelled;

const isPlaying = () => ({ status }) => status === STATUSES.playing;

const isReady = () => ({ status, waiting }) => {
  if (status !== STATUSES.waiting) return false;
  if (waiting.length !== 0) return false;
  return true;
}

const player = ({ id, username, image }) => ({
  id,
  username,
  image,
});

const emptyGame = () => (name, user, invitations, settings) => {
  if (!roles.validateRoles(settings))
    throw { message: "Invalid role settings" };

  const isGameValid = games.validateGame({
    players: [...invitations, user],
    roles: settings
  });

  if (!isGameValid)
    throw { message: "Invalid game settings" };

  if (invitations.indexOf(user.id) !== -1)
    throw { message: "Cannot invite yourself" };

  const host = player(user);

  return {
    name,
    status: STATUSES.waiting,
    by: host,
    waiting: invitations,
    ready: [host],
    settings,
    result: null,
    details: {}
  };
}

const cancelGame = () => () => ({
  status: STATUSES.cancelled,
});

const startGame = () => ({ id, ready, settings }) => ({
  status: STATUSES.playing,
  details: games.initializeGame(id, ready, settings),
});

const newPlayer = () => userData => player(userData);

module.exports = {
  name,
  functions: {
    newPlayer,
    isCancelled,
    isPlaying,
    isReady,
    emptyGame,
    cancelGame,
    startGame,
  }
};