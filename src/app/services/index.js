module.exports = [
    require('./ids'),
    require('./games'),
    require('./time'),
    require("./formatter"),
];