const extractIds = players => players.map(({ id }) => id);

const formatGame = ({
  _id,
  id,
  name,
  by,
  status,
  waiting,
  ready,
  result,
  at
}) => ({
  id: _id || id,
  name,
  by,
  status,
  waiting: waiting.length,
  ready: ready.length,
  result,
  at,
});

const formatInvitation = ({
  _id,
  id,
  name,
  by,
  at,
}) => ({
  id: _id || id,
  name,
  by,
  at,
});

// Game-Ready data
const formatGameDetails = ({
  _id,
  id,
  name,
  result,
  details,
}) => ({
  id: _id || id,
  name,
  result,
  ...details,
});

const formatGamesList = games => games.map(formatGame);

const formatInvitationsList = games => games.map(formatInvitation);

// Notifications / Events data
const formatChangedGameData = ({
  _id,
  id,
  name,
  by,
  ready,
  waiting,
}) => ({
  id: _id || id,
  name,
  by,
  waiting,
  ready: extractIds(ready),
});

const formatInvitationModifiedData = (game, user) => ({
  game: {
    id: game.id,
    name: game.name,
    by: game.by,
    waiting: game.waiting,
    ready: extractIds(game.ready),
  },
  user,
});

const name = "formatter";

module.exports = {
  name,
  functions: {
    game: () => formatGame,
    invitation: () => formatInvitation,
    games: () => formatGamesList,
    invitations: () => formatInvitationsList,
    gameDetails: () => formatGameDetails,
    gameChanged: () => formatChangedGameData,
    invitationModified: () => formatInvitationModifiedData,
  }
};