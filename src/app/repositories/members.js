const name = "members";
const datasource = "database";
const transactional = false;

const COLLECTION = "games";

const addReady = mongodb => async (gameId, playerId, playerData) => {
  const result = await mongodb.collection(COLLECTION).updateOne(
    { _id: gameId },
    {
      $push: { ready: playerData }, // Object -> Not using Sets ("$addToSet")
      $pull: { waiting: playerId }
    }
  );

  if (result.modifiedCount !== 1) throw new Error("Player not added");
};

const removeWaiting = mongodb => async (gameId, playerId) => {
  const result = await mongodb.collection(COLLECTION).updateOne(
    { _id: gameId },
    {
      $pull: { waiting: playerId }
    }
  );

  if (result.modifiedCount !== 1) throw new Error("Waiting player not removed");
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    addReady,
    removeWaiting
  },
};