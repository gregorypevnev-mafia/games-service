const name = "games";
const datasource = "database";
const transactional = false;

const COLLECTION = "games";

const find = mongodb => async id => {
  try {
    const { _id, ...data } = await mongodb.collection(COLLECTION).findOne({ _id: id });

    return {
      id: _id,
      ...data,
    };
  } catch (e) {
    return null;
  }
}

const create = mongodb => async ({ id, ...data }) => {
  const result = await mongodb.collection(COLLECTION).insertOne({
    _id: id,
    ...data,
  });

  if (result.insertedCount !== 1) throw new Error("Game not created");
};

const remove = mongodb => async id => {
  const result = await mongodb.collection(COLLECTION).deleteOne({
    _id: id,
  });

  if (result.deletedCount !== 1) throw new Error("Game not deleted");
};

const update = mongodb => async (id, data) => {
  const result = await mongodb.collection(COLLECTION).updateOne(
    { _id: id },
    { $set: data },
    { upsert: false }
  );

  if (result.modifiedCount !== 1) throw new Error("Game not updated");
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    find,
    create,
    remove,
    update
  },
};