const yup = require("yup");

const name = "game-data";

const schema = yup.object({
  name: yup.string()
    .max(20, "Name of the game is too long")
    .required("Name of the game is required"),
  invitations: yup.array(
    yup.string().required("ID of the player must be supplied")
  ).required("List of users to invite is required"),
  // Validated through Business-Logic
  settings: yup.object().required("Settings for the game are required")
});

module.exports = {
  name,
  schema,
};