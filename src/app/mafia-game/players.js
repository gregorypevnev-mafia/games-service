const { PLAYER_ALIVE, PLAYER_DEAD, PLAYER_EXPOSED } = require("./common/constants");
const { phaseDetails } = require("./common/phases");
const { roleName, populateRoles } = require("./roles");
const { shuffleList } = require("./utils");

const PLAYER_STATUS_NAMES = {
  [PLAYER_ALIVE]: "Alive",
  [PLAYER_DEAD]: "Dead",
  [PLAYER_EXPOSED]: "Exposed",
};

const playerInfo = ({ id, details, role, status }) => ({
  id,
  role: roleName(role),
  status: String(PLAYER_STATUS_NAMES[status]),
  ...details,
});

const isPlayerActive = ({ phase }, { role, status }) => {
  if (status === PLAYER_DEAD) return false;

  const phaseRole = phaseDetails(phase).role;

  return phaseRole === null || phaseRole === role;
};

const initializePlayers = (players, roles) => {
  const rolesList = shuffleList(populateRoles(roles));

  return players.reduce((initialized, { id, username, image }, i) => ({
    ...initialized,
    [id]: {
      id,
      username,
      image,
      status: PLAYER_ALIVE,
      role: rolesList[i],
    },
  }), {});
};

module.exports = {
  playerInfo,
  isPlayerActive,
  initializePlayers,
};
