const { MongoClient } = require("mongodb");

const HOST = String(process.env.DATABASE_HOST);
const PORT = Number(process.env.DATABASE_PORT);
const USERNAME = String(process.env.DATABASE_USER);
const PASSWORD = String(process.env.DATABASE_PASSWORD);
const DATABASE = "games";
const RS = "rs";

const INDEX_SPECS = [
  {
    name: "Waiting-Index",
    key: { "waiting": 1 }
  },
  {
    name: "Ready-Index",
    key: { "ready.id": 1 }
  }
];

const URL = `mongodb://${USERNAME}:${PASSWORD}@${HOST}:${PORT}/admin`;

console.log("Connecting to", URL);

MongoClient.connect(
  URL,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    replicaSet: RS, // Important: Redirects to Primary-Node using supplied settings -> Use "hosts" to redirect Primary-Node Host to "localhost"
  },
  (error, client) => {
    if (error) {
      console.log("ERROR:", error);
      process.exit(1);
    }

    const db = client.db(DATABASE);

    db.collection("games").createIndexes(INDEX_SPECS, {}, error => {
      if (error) {
        console.log("ERROR:", error);
        process.exit(1);
      }

      console.log("Indexes created");

      client.close();
    });
  }
);